#include <stdlib.h>
#include <assert.h>
#include <time.h>
/* alignment macro: aligns a memory block a to multiplies of a */
#define align(s,a) (((size_t)(s) + ((a) - 1)) & ~((size_t) (a) - 1))
/* Alignment for SSE unit */
#define SSE_ALIGN (16)
/* Number of elements */
#define NUM (100)

/* Testing if statements and conditions */
extern void if_cond(long, float *, float *, float *);

int main(void) {
	srand(time(NULL));
	float *a = malloc(sizeof(float)*NUM + SSE_ALIGN),
		  *b = malloc(sizeof(float)*NUM + SSE_ALIGN),
		  *c = malloc(sizeof(float)*NUM + SSE_ALIGN),
		  *d = malloc(sizeof(float)*NUM + SSE_ALIGN);
	/* make sure that pointers are aligned to multiples of 16 bytes */
	a = (float *) align(a, SSE_ALIGN);
	b = (float *) align(b, SSE_ALIGN);
	c = (float *) align(c, SSE_ALIGN);
	d = (float *) align(d, SSE_ALIGN);

	/* write values to a, b, c */
	for (int i = 0; i < NUM; i++) {
		a[i] = rand() % 5;
		b[i] = rand() % 5;
	}

	/* invoke the function written in the vector language */
	if_cond(NUM, a, b, c);

	/* reproduce function for assertion */
	float sum = 0.0;
	for (int i = 0; i < NUM; i++) {
		d[i] = 0.0;
		if (a[i] < b[i]) {
			sum = sum + a[i];
		} else {
			sum = sum + b[i];
		}
	}

	if (sum >= 100) {
		for (int i = 0; i < NUM; i++) {
			d[i] = a[i];
		}
	} else if (sum < 70) {
		for (int i = 0; i < NUM; i++) {
			d[i] = b[i];
		}
	}

	/* read values from c */
	for (int i = 0; i < NUM; i++) {
		assert(c[i] == d[i]);
	}
	return 0;
}