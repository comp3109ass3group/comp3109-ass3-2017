#include <stdlib.h>
#include <assert.h>
/* alignment macro: aligns a memory block a to multiplies of a */
#define align(s,a) (((size_t)(s) + ((a) - 1)) & ~((size_t) (a) - 1))
/* Alignment for SSE unit */
#define SSE_ALIGN (16)
/* Number of elements */
#define NUM (100)

/* Sample test case */
extern void mymin(long, float *, float *, float *);

int main(void) {
	float *a = malloc(sizeof(float)*NUM + SSE_ALIGN),
		  *b = malloc(sizeof(float)*NUM + SSE_ALIGN),
		  *c = malloc(sizeof(float)*NUM + SSE_ALIGN),
		  *d = malloc(sizeof(float)*NUM + SSE_ALIGN);
	/* make sure that pointers are aligned to multiples of 16 bytes */
	a = (float *) align(a, SSE_ALIGN);
	b = (float *) align(b, SSE_ALIGN);
	c = (float *) align(c, SSE_ALIGN);
	d = (float *) align(d, SSE_ALIGN);

	/* write values to a, b, c, d */
	for (int i = 0; i < NUM; i++) {
		a[i] = i;
		b[i] = NUM - i;
	}
	/* invoke the function written in the vector language */
	mymin(NUM, a, b, c);

	/* reproduce function for assertion */
	for (int i = 0; i < NUM; i++) {
		if (a[i] < b[i]) {
			d[i] = a[i] + 20;
		} else {
			d[i] = b[i] + 20;
		}
	}

	/* read values from c */
	for (int i = 0; i < NUM; i++) {
		assert(d[i] == c[i]);
	}

	free(a);
	free(b);
	free(c);
	free(d);
	return 0;
}