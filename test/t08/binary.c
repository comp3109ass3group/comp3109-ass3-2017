#include <stdlib.h>
#include <assert.h>
#include <time.h>
/* alignment macro: aligns a memory block a to multiplies of a */
#define align(s,a) (((size_t)(s) + ((a) - 1)) & ~((size_t) (a) - 1))
/* Alignment for SSE unit */
#define SSE_ALIGN (16)
/* Number of elements */
#define NUM (100)

/* Testing while loop */
extern void binary(long, float *);

int main(void) {
	srand(time(NULL));
	float *a = malloc(sizeof(float)*NUM + SSE_ALIGN),
		  *b = malloc(sizeof(float)*NUM + SSE_ALIGN);
	/* make sure that pointers are aligned to multiples of 16 bytes */
	a = (float *) align(a, SSE_ALIGN);
	b = (float *) align(b, SSE_ALIGN);

	/* write values to a, b, c */
	for (int i = 0; i < NUM; i++) {
		a[i] = rand() % 100;
		b[i] = a[i];
	}

	/* invoke the function written in the vector language */
	binary(NUM, a);

	/* reproduce function for assertion */
	float sum = 0;
	int count = 0;
	for (int i = 0; i < NUM; i++) {
		sum = sum + b[i] + count;
	}
	while (sum < 10000) {
		sum = 0.0;
		count = count + 1;
		for (int i = 0; i < NUM; i++) {
			sum = sum + b[i] + count;

		}
	}
	for (int i = 0; i < NUM; i++) {
		b[i] = b[i] + count;
	}

	/* read values from c */
	for (int i = 0; i < NUM; i++) {
		assert(a[i] == b[i]);
	}

	free(a);
	free(b);
	return 0;
}