#include <stdlib.h>
#include <assert.h>
#include <time.h>
/* alignment macro: aligns a memory block a to multiplies of a */
#define align(s,a) (((size_t)(s) + ((a) - 1)) & ~((size_t) (a) - 1))
/* Alignment for SSE unit */
#define SSE_ALIGN (16)
/* Number of elements */
#define NUM (100)

/* Testing format, maximum vector parameters */
extern void op(long, float *, float *, float *, float *, float *);

int main(void) {
	srand(time(NULL));
	float *a = malloc(sizeof(float)*NUM + SSE_ALIGN),
		  *b = malloc(sizeof(float)*NUM + SSE_ALIGN),
		  *c = malloc(sizeof(float)*NUM + SSE_ALIGN),
		  *d = malloc(sizeof(float)*NUM + SSE_ALIGN),
		  *e = malloc(sizeof(float)*NUM + SSE_ALIGN),
		  *f = malloc(sizeof(float)*NUM + SSE_ALIGN);
	/* make sure that pointers are aligned to multiples of 16 bytes */
	a = (float *) align(a, SSE_ALIGN);
	b = (float *) align(b, SSE_ALIGN);
	c = (float *) align(c, SSE_ALIGN);
	d = (float *) align(d, SSE_ALIGN);
	e = (float *) align(e, SSE_ALIGN);

	/* write values to a, b, c */
	for (int i = 0; i < NUM; i++) {
		a[i] = (rand() % 10000 + 1) / 100.0;
		b[i] = (rand() % 10000 + 1) / 100.0;
		c[i] = (rand() % 10000 + 1) / 100.0;
		d[i] = (rand() % 10000 + 1) / 100.0;
		e[i] = (rand() % 10000 + 1) / 100.0;
	}

	/* reproduce function for assertion */
	for (int i = 0; i < NUM; i++) {
		if (e[i] >= 50.01) {
			f[i] = e[i] * d[i] / c[i] - b[i] + a[i];
		} else {
			f[i] = 50.01 * d[i] / c[i] - b[i] + a[i];
		}
	}

	/* invoke the function written in the vector language */
	op(NUM, a, b, c, d, e);

	/* read values from c */
	for (int i = 0; i < NUM; i++) {
		assert(e[i] == f[i]);
	}

	free(a);
	free(b);
	free(c);
	free(d);
	free(e);
	free(f);
	return 0;
}