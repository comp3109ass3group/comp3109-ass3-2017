#include <stdlib.h>
#include <assert.h>
/* alignment macro: aligns a memory block a to multiplies of a */
#define align(s,a) (((size_t)(s) + ((a) - 1)) & ~((size_t) (a) - 1))
/* Alignment for SSE unit */
#define SSE_ALIGN (16)
/* Number of elements */
#define NUM (100)

/* Testing minimal VPL grammar */
extern void x(long, float *);

int main(void) {
	float *a = malloc(sizeof(float)*NUM + SSE_ALIGN),
		  *b = malloc(sizeof(float)*NUM + SSE_ALIGN);
	/* make sure that pointers are aligned to multiples of 16 bytes */
	a = (float *) align(a, SSE_ALIGN);
	b = (float *) align(b, SSE_ALIGN);

	/* write values to a and b */
	for (int i = 0; i < NUM; i++) {
		a[i] = i;
		b[i] = i;
	}
	/* invoke the function written in the vector language */
	x(NUM, a);

	/* read values from c */
	for (int i = 0; i < NUM; i++) {
		assert(a[i] == b[i]);
	}

	free(a);
	free(b);
	return 0;
}