#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include <time.h>
/* alignment macro: aligns a memory block a to multiplies of a */
#define align(s,a) (((size_t)(s) + ((a) - 1)) & ~((size_t) (a) - 1))
/* Alignment for SSE unit */
#define SSE_ALIGN (16)
/* Number of elements */
#define NUM (1000)

/* More complex test case */
extern void long_while(long, float *);
extern void long_if(long, float *);

float sum(float *a) {
	float sum = 0.0;
	for (int i = 0; i < NUM; i++) {
		sum = sum + a[i];
	}
	return sum;
}

int main(void) {
	srand(time(NULL));
	float *a = malloc(sizeof(float)*NUM + SSE_ALIGN),
		  *b = malloc(sizeof(float)*NUM + SSE_ALIGN),
		  *c = malloc(sizeof(float)*NUM + SSE_ALIGN),
		  *d = malloc(sizeof(float)*NUM + SSE_ALIGN);
	/* make sure that pointers are aligned to multiples of 16 bytes */
	a = (float *) align(a, SSE_ALIGN);
	b = (float *) align(b, SSE_ALIGN);
	c = (float *) align(c, SSE_ALIGN);
	d = (float *) align(d, SSE_ALIGN);

	/* write values to a, b, c */
	for (int i = 0; i < NUM; i++) {
		a[i] = (rand() % 10000) / 12.0;
		b[i] = (rand() % 1000 + 0.01) / 6800.0;
		printf("%f\n", b[i]);
		c[i] = a[i];
		d[i] = b[i];
	}

	/* invoke the function written in the vector language */
	long_while(NUM, a);
	long_if(NUM, b);

	/* reproduce function for assertion */
	float sum_c = sum(c);
	float sum_minus = 0.0;
	while (sum_c - (sum_minus * NUM) >= 0.0000005) {
		for (int i = 0 ; i < 50; i++) {
			for (int j = 0; j < 40; j++) {
				sum_minus = sum_minus + 0.001;
			}
		}
	}
	for (int i = 0; i < NUM; i++) {
		c[i] = c[i] - sum_minus;
	}

	float sum_d = sum(d);
	//printf("%f", sum_d);
	if (sum_d < 50) {
		if (sum_d < 25) {
			if (sum_d < 5) {
				for (int i = 0; i < NUM; i++) {
					d[i] = d[i] / 2.0;
				}
			}
		} else {
			for (int i = 0; i < NUM; i++) {
					d[i] = d[i] * 2.0;
			}
		}
	} else {
		for (int i = 0; i < NUM; i++) {
			d[i] = d[i] * d[i];
		}
		if (sum(d) < 0.0002) {
			for (int i = 0; i < NUM; i++) {
				d[i] = d[i] / d[i];
			}
		}
		if (sum_d >= 75) {
			for (int i = 0; i < NUM; i++) {
				d[i] = 0;
			}
		}
	}

	/* read values from c */
	for (int i = 0; i < NUM; i++) {
		assert(a[i] == c[i]);
		assert(b[i] == d[i]);
		//printf("%f %f\n", c[i], d[i]);
	}

	free(a);
	free(b);
	free(c);
	free(d);
	return 0;
}