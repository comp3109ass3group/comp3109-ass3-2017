#include <stdlib.h>
#include <assert.h>
/* alignment macro: aligns a memory block a to multiplies of a */
#define align(s,a) (((size_t)(s) + ((a) - 1)) & ~((size_t) (a) - 1))
/* Alignment for SSE unit */
#define SSE_ALIGN (16)
/* Number of elements */
#define NUM (100)

/* Testing addition, subtraction */
extern void addsub(long, float *, float *);

int main(void) {
	float *a = malloc(sizeof(float)*NUM + SSE_ALIGN),
		  *b = malloc(sizeof(float)*NUM + SSE_ALIGN),
		  *c = malloc(sizeof(float)*NUM + SSE_ALIGN);
	/* make sure that pointers are aligned to multiples of 16 bytes */
	a = (float *) align(a, SSE_ALIGN);
	b = (float *) align(b, SSE_ALIGN);
	c = (float *) align(c, SSE_ALIGN);

	/* write values to a, b, c */
	for (int i = 0; i < NUM; i++) {
		a[i] = i + (i/10);
		b[i] = i + 100;
		c[i] = a[i];
	}
	/* invoke the function written in the vector language */
	addsub(NUM, a, b);

	/* read values from c */
	for (int i = 0; i < NUM; i++) {
		assert(a[i] == c[i]);
	}

	free(a);
	free(b);
	free(c);
	return 0;
}