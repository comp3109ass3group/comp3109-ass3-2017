#include <stdlib.h>
#include <assert.h>
#include <time.h>
/* alignment macro: aligns a memory block a to multiplies of a */
#define align(s,a) (((size_t)(s) + ((a) - 1)) & ~((size_t) (a) - 1))
/* Alignment for SSE unit */
#define SSE_ALIGN (16)
/* Number of elements */
#define NUM (100)

/* Testing while loop */
extern void total(long, float *);

int main(void) {
	srand(time(NULL));
	float *a = malloc(sizeof(float)*NUM + SSE_ALIGN),
		  *b = malloc(sizeof(float)*NUM + SSE_ALIGN);
	/* make sure that pointers are aligned to multiples of 16 bytes */
	a = (float *) align(a, SSE_ALIGN);
	b = (float *) align(b, SSE_ALIGN);

	/* write values to a, b, c */
	for (int i = 0; i < NUM; i++) {
		a[i] = rand() % 50;
		b[i] = a[i];
	}

	/* invoke the function written in the vector language */
	total(NUM, a);

	/* reproduce function for assertion */
	float sum = 0;
	for (int i = 0; i < NUM; i++) {
		sum = sum + b[i];
	}
	while (sum > 1.0) {
		sum = 0.0;
		for (int i = 0; i < NUM; i++) {
			b[i] = b[i] / 10.0;
			sum = sum + b[i];
		}
	}

	/* read values from c */
	for (int i = 0; i < NUM; i++) {
		assert(a[i] == b[i]);
	}
	
	free(a);
	free(b);
	return 0;
}