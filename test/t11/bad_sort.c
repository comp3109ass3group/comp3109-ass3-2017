#include <stdlib.h>
#include <assert.h>
#include <time.h>
/* alignment macro: aligns a memory block a to multiplies of a */
#define align(s,a) (((size_t)(s) + ((a) - 1)) & ~((size_t) (a) - 1))
/* Alignment for SSE unit */
#define SSE_ALIGN (16)
/* Number of elements */
#define NUM (1000)

/* More complex test case */
extern void bad_sort(long, float *, float *, float *, float *, float *);

int main(void) {
	srand(time(NULL));
	float *a = malloc(sizeof(float)*NUM + SSE_ALIGN),
		  *b = malloc(sizeof(float)*NUM + SSE_ALIGN),
		  *c = malloc(sizeof(float)*NUM + SSE_ALIGN),
		  *d = malloc(sizeof(float)*NUM + SSE_ALIGN),
		  *e = malloc(sizeof(float)*NUM + SSE_ALIGN);
	/* make sure that pointers are aligned to multiples of 16 bytes */
	a = (float *) align(a, SSE_ALIGN);
	b = (float *) align(b, SSE_ALIGN);
	c = (float *) align(c, SSE_ALIGN);
	d = (float *) align(d, SSE_ALIGN);
	e = (float *) align(e, SSE_ALIGN);

	/* write values to a, b, c */
	for (int i = 0; i < NUM; i++) {
		a[i] = (rand() % 10000) / 100.0;
		b[i] = (rand() % 10000) / 100.0;
		c[i] = (rand() % 10000) / 100.0;
		d[i] = (rand() % 10000) / 100.0;
		e[i] = (rand() % 10000) / 100.0;
	}

	/* invoke the function written in the vector language */
	bad_sort(NUM, a, b, c, d, e);

	/* reproduce function for assertion */
	float sum[5] = {0.0};
	for (int i = 0; i < NUM; i++) {
		sum[0] = sum[0] + a[i];
		sum[1] = sum[1] + b[i];
		sum[2] = sum[2] + c[i];
		sum[3] = sum[3] + d[i];
		sum[4] = sum[4] + e[i];
	}

	/* read values from c */
	for (int i = 0; i < 4; i++) {
		assert(sum[i] <= sum[i+1]);
	}

	free(a);
	free(b);
	free(c);
	free(d);
	free(e);
	return 0;
}