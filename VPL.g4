grammar VPL;

m   : f m EOF | ;
f   : 'func' IDENT p d s 'end';
p   : '(' l ')';
l   : IDENT | IDENT ',' l;
d   :  'var' l ';' | ;
s   : 'if' c 'then' s 'endif'
    | 'while' c 'do' s 'endwhile'
    | s ';' s
    | IDENT '=' e
    | ;
e   : 'add' '(' e ',' e ')'
    | 'minus' '(' e ',' e ')'
    | 'mult' '(' e ',' e ')'
    | 'div' '(' e ',' e ')'
    | 'min' '(' e ',' e ')'
    | '(' e ')'
    | IDENT
    | NUM;
c   : e '<' NUM
    | e '>=' NUM;

IDENT : [a-zA-Z_][a-zA-Z0-9_]*;
NUM   : [0-9]+('.'[0-9]+)?;
WS : [ \t\r\n]+ -> skip ;

