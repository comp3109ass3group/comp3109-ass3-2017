# Generated from VPL.g4 by ANTLR 4.7
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .VPLParser import VPLParser
else:
    from VPLParser import VPLParser

# This class defines a complete listener for a parse tree produced by VPLParser.
class VPLListener(ParseTreeListener):

    # Enter a parse tree produced by VPLParser#m.
    def enterM(self, ctx:VPLParser.MContext):
        pass

    # Exit a parse tree produced by VPLParser#m.
    def exitM(self, ctx:VPLParser.MContext):
        pass


    # Enter a parse tree produced by VPLParser#f.
    def enterF(self, ctx:VPLParser.FContext):
        pass

    # Exit a parse tree produced by VPLParser#f.
    def exitF(self, ctx:VPLParser.FContext):
        pass


    # Enter a parse tree produced by VPLParser#p.
    def enterP(self, ctx:VPLParser.PContext):
        pass

    # Exit a parse tree produced by VPLParser#p.
    def exitP(self, ctx:VPLParser.PContext):
        pass


    # Enter a parse tree produced by VPLParser#l.
    def enterL(self, ctx:VPLParser.LContext):
        pass

    # Exit a parse tree produced by VPLParser#l.
    def exitL(self, ctx:VPLParser.LContext):
        pass


    # Enter a parse tree produced by VPLParser#d.
    def enterD(self, ctx:VPLParser.DContext):
        pass

    # Exit a parse tree produced by VPLParser#d.
    def exitD(self, ctx:VPLParser.DContext):
        pass


    # Enter a parse tree produced by VPLParser#s.
    def enterS(self, ctx:VPLParser.SContext):
        pass

    # Exit a parse tree produced by VPLParser#s.
    def exitS(self, ctx:VPLParser.SContext):
        pass


    # Enter a parse tree produced by VPLParser#e.
    def enterE(self, ctx:VPLParser.EContext):
        pass

    # Exit a parse tree produced by VPLParser#e.
    def exitE(self, ctx:VPLParser.EContext):
        pass


    # Enter a parse tree produced by VPLParser#c.
    def enterC(self, ctx:VPLParser.CContext):
        pass

    # Exit a parse tree produced by VPLParser#c.
    def exitC(self, ctx:VPLParser.CContext):
        pass


