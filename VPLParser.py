# Generated from VPL.g4 by ANTLR 4.7
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys

def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\32")
        buf.write("\u0082\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\3\2\3\2\3\2\3\2\3\2\5\2\30\n\2\3\3\3")
        buf.write("\3\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5")
        buf.write("\5\5)\n\5\3\6\3\6\3\6\3\6\3\6\5\6\60\n\6\3\7\3\7\3\7\3")
        buf.write("\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7")
        buf.write("\5\7C\n\7\3\7\3\7\3\7\7\7H\n\7\f\7\16\7K\13\7\3\b\3\b")
        buf.write("\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3")
        buf.write("\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b")
        buf.write("\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\5\bv")
        buf.write("\n\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\5\t\u0080\n\t\3\t")
        buf.write("\2\3\f\n\2\4\6\b\n\f\16\20\2\2\2\u0088\2\27\3\2\2\2\4")
        buf.write("\31\3\2\2\2\6 \3\2\2\2\b(\3\2\2\2\n/\3\2\2\2\fB\3\2\2")
        buf.write("\2\16u\3\2\2\2\20\177\3\2\2\2\22\23\5\4\3\2\23\24\5\2")
        buf.write("\2\2\24\25\7\2\2\3\25\30\3\2\2\2\26\30\3\2\2\2\27\22\3")
        buf.write("\2\2\2\27\26\3\2\2\2\30\3\3\2\2\2\31\32\7\3\2\2\32\33")
        buf.write("\7\30\2\2\33\34\5\6\4\2\34\35\5\n\6\2\35\36\5\f\7\2\36")
        buf.write("\37\7\4\2\2\37\5\3\2\2\2 !\7\5\2\2!\"\5\b\5\2\"#\7\6\2")
        buf.write("\2#\7\3\2\2\2$)\7\30\2\2%&\7\30\2\2&\'\7\7\2\2\')\5\b")
        buf.write("\5\2($\3\2\2\2(%\3\2\2\2)\t\3\2\2\2*+\7\b\2\2+,\5\b\5")
        buf.write("\2,-\7\t\2\2-\60\3\2\2\2.\60\3\2\2\2/*\3\2\2\2/.\3\2\2")
        buf.write("\2\60\13\3\2\2\2\61\62\b\7\1\2\62\63\7\n\2\2\63\64\5\20")
        buf.write("\t\2\64\65\7\13\2\2\65\66\5\f\7\2\66\67\7\f\2\2\67C\3")
        buf.write("\2\2\289\7\r\2\29:\5\20\t\2:;\7\16\2\2;<\5\f\7\2<=\7\17")
        buf.write("\2\2=C\3\2\2\2>?\7\30\2\2?@\7\20\2\2@C\5\16\b\2AC\3\2")
        buf.write("\2\2B\61\3\2\2\2B8\3\2\2\2B>\3\2\2\2BA\3\2\2\2CI\3\2\2")
        buf.write("\2DE\f\5\2\2EF\7\t\2\2FH\5\f\7\6GD\3\2\2\2HK\3\2\2\2I")
        buf.write("G\3\2\2\2IJ\3\2\2\2J\r\3\2\2\2KI\3\2\2\2LM\7\21\2\2MN")
        buf.write("\7\5\2\2NO\5\16\b\2OP\7\7\2\2PQ\5\16\b\2QR\7\6\2\2Rv\3")
        buf.write("\2\2\2ST\7\22\2\2TU\7\5\2\2UV\5\16\b\2VW\7\7\2\2WX\5\16")
        buf.write("\b\2XY\7\6\2\2Yv\3\2\2\2Z[\7\23\2\2[\\\7\5\2\2\\]\5\16")
        buf.write("\b\2]^\7\7\2\2^_\5\16\b\2_`\7\6\2\2`v\3\2\2\2ab\7\24\2")
        buf.write("\2bc\7\5\2\2cd\5\16\b\2de\7\7\2\2ef\5\16\b\2fg\7\6\2\2")
        buf.write("gv\3\2\2\2hi\7\25\2\2ij\7\5\2\2jk\5\16\b\2kl\7\7\2\2l")
        buf.write("m\5\16\b\2mn\7\6\2\2nv\3\2\2\2op\7\5\2\2pq\5\16\b\2qr")
        buf.write("\7\6\2\2rv\3\2\2\2sv\7\30\2\2tv\7\31\2\2uL\3\2\2\2uS\3")
        buf.write("\2\2\2uZ\3\2\2\2ua\3\2\2\2uh\3\2\2\2uo\3\2\2\2us\3\2\2")
        buf.write("\2ut\3\2\2\2v\17\3\2\2\2wx\5\16\b\2xy\7\26\2\2yz\7\31")
        buf.write("\2\2z\u0080\3\2\2\2{|\5\16\b\2|}\7\27\2\2}~\7\31\2\2~")
        buf.write("\u0080\3\2\2\2\177w\3\2\2\2\177{\3\2\2\2\u0080\21\3\2")
        buf.write("\2\2\t\27(/BIu\177")
        return buf.getvalue()


class VPLParser ( Parser ):

    grammarFileName = "VPL.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'func'", "'end'", "'('", "')'", "','", 
                     "'var'", "';'", "'if'", "'then'", "'endif'", "'while'", 
                     "'do'", "'endwhile'", "'='", "'add'", "'minus'", "'mult'", 
                     "'div'", "'min'", "'<'", "'>='" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "IDENT", "NUM", "WS" ]

    RULE_m = 0
    RULE_f = 1
    RULE_p = 2
    RULE_l = 3
    RULE_d = 4
    RULE_s = 5
    RULE_e = 6
    RULE_c = 7

    ruleNames =  [ "m", "f", "p", "l", "d", "s", "e", "c" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    T__3=4
    T__4=5
    T__5=6
    T__6=7
    T__7=8
    T__8=9
    T__9=10
    T__10=11
    T__11=12
    T__12=13
    T__13=14
    T__14=15
    T__15=16
    T__16=17
    T__17=18
    T__18=19
    T__19=20
    T__20=21
    IDENT=22
    NUM=23
    WS=24

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



    class MContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def f(self):
            return self.getTypedRuleContext(VPLParser.FContext,0)


        def m(self):
            return self.getTypedRuleContext(VPLParser.MContext,0)


        def EOF(self):
            return self.getToken(VPLParser.EOF, 0)

        def getRuleIndex(self):
            return VPLParser.RULE_m

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterM" ):
                listener.enterM(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitM" ):
                listener.exitM(self)




    def m(self):

        localctx = VPLParser.MContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_m)
        try:
            self.state = 21
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [VPLParser.T__0]:
                self.enterOuterAlt(localctx, 1)
                self.state = 16
                self.f()
                self.state = 17
                self.m()
                self.state = 18
                self.match(VPLParser.EOF)
                pass
            elif token in [VPLParser.EOF]:
                self.enterOuterAlt(localctx, 2)

                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class FContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def IDENT(self):
            return self.getToken(VPLParser.IDENT, 0)

        def p(self):
            return self.getTypedRuleContext(VPLParser.PContext,0)


        def d(self):
            return self.getTypedRuleContext(VPLParser.DContext,0)


        def s(self):
            return self.getTypedRuleContext(VPLParser.SContext,0)


        def getRuleIndex(self):
            return VPLParser.RULE_f

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterF" ):
                listener.enterF(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitF" ):
                listener.exitF(self)




    def f(self):

        localctx = VPLParser.FContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_f)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 23
            self.match(VPLParser.T__0)
            self.state = 24
            self.match(VPLParser.IDENT)
            self.state = 25
            self.p()
            self.state = 26
            self.d()
            self.state = 27
            self.s(0)
            self.state = 28
            self.match(VPLParser.T__1)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class PContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def l(self):
            return self.getTypedRuleContext(VPLParser.LContext,0)


        def getRuleIndex(self):
            return VPLParser.RULE_p

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterP" ):
                listener.enterP(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitP" ):
                listener.exitP(self)




    def p(self):

        localctx = VPLParser.PContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 30
            self.match(VPLParser.T__2)
            self.state = 31
            self.l()
            self.state = 32
            self.match(VPLParser.T__3)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class LContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def IDENT(self):
            return self.getToken(VPLParser.IDENT, 0)

        def l(self):
            return self.getTypedRuleContext(VPLParser.LContext,0)


        def getRuleIndex(self):
            return VPLParser.RULE_l

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterL" ):
                listener.enterL(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitL" ):
                listener.exitL(self)




    def l(self):

        localctx = VPLParser.LContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_l)
        try:
            self.state = 38
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,1,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 34
                self.match(VPLParser.IDENT)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 35
                self.match(VPLParser.IDENT)
                self.state = 36
                self.match(VPLParser.T__4)
                self.state = 37
                self.l()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class DContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def l(self):
            return self.getTypedRuleContext(VPLParser.LContext,0)


        def getRuleIndex(self):
            return VPLParser.RULE_d

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterD" ):
                listener.enterD(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitD" ):
                listener.exitD(self)




    def d(self):

        localctx = VPLParser.DContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_d)
        try:
            self.state = 45
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,2,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 40
                self.match(VPLParser.T__5)
                self.state = 41
                self.l()
                self.state = 42
                self.match(VPLParser.T__6)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)

                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class SContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def c(self):
            return self.getTypedRuleContext(VPLParser.CContext,0)


        def s(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(VPLParser.SContext)
            else:
                return self.getTypedRuleContext(VPLParser.SContext,i)


        def IDENT(self):
            return self.getToken(VPLParser.IDENT, 0)

        def e(self):
            return self.getTypedRuleContext(VPLParser.EContext,0)


        def getRuleIndex(self):
            return VPLParser.RULE_s

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterS" ):
                listener.enterS(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitS" ):
                listener.exitS(self)



    def s(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = VPLParser.SContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 10
        self.enterRecursionRule(localctx, 10, self.RULE_s, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 64
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,3,self._ctx)
            if la_ == 1:
                self.state = 48
                self.match(VPLParser.T__7)
                self.state = 49
                self.c()
                self.state = 50
                self.match(VPLParser.T__8)
                self.state = 51
                self.s(0)
                self.state = 52
                self.match(VPLParser.T__9)
                pass

            elif la_ == 2:
                self.state = 54
                self.match(VPLParser.T__10)
                self.state = 55
                self.c()
                self.state = 56
                self.match(VPLParser.T__11)
                self.state = 57
                self.s(0)
                self.state = 58
                self.match(VPLParser.T__12)
                pass

            elif la_ == 3:
                self.state = 60
                self.match(VPLParser.IDENT)
                self.state = 61
                self.match(VPLParser.T__13)
                self.state = 62
                self.e()
                pass

            elif la_ == 4:
                pass


            self._ctx.stop = self._input.LT(-1)
            self.state = 71
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,4,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = VPLParser.SContext(self, _parentctx, _parentState)
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_s)
                    self.state = 66
                    if not self.precpred(self._ctx, 3):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 3)")
                    self.state = 67
                    self.match(VPLParser.T__6)
                    self.state = 68
                    self.s(4) 
                self.state = 73
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,4,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx

    class EContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def e(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(VPLParser.EContext)
            else:
                return self.getTypedRuleContext(VPLParser.EContext,i)


        def IDENT(self):
            return self.getToken(VPLParser.IDENT, 0)

        def NUM(self):
            return self.getToken(VPLParser.NUM, 0)

        def getRuleIndex(self):
            return VPLParser.RULE_e

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterE" ):
                listener.enterE(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitE" ):
                listener.exitE(self)




    def e(self):

        localctx = VPLParser.EContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_e)
        try:
            self.state = 115
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [VPLParser.T__14]:
                self.enterOuterAlt(localctx, 1)
                self.state = 74
                self.match(VPLParser.T__14)
                self.state = 75
                self.match(VPLParser.T__2)
                self.state = 76
                self.e()
                self.state = 77
                self.match(VPLParser.T__4)
                self.state = 78
                self.e()
                self.state = 79
                self.match(VPLParser.T__3)
                pass
            elif token in [VPLParser.T__15]:
                self.enterOuterAlt(localctx, 2)
                self.state = 81
                self.match(VPLParser.T__15)
                self.state = 82
                self.match(VPLParser.T__2)
                self.state = 83
                self.e()
                self.state = 84
                self.match(VPLParser.T__4)
                self.state = 85
                self.e()
                self.state = 86
                self.match(VPLParser.T__3)
                pass
            elif token in [VPLParser.T__16]:
                self.enterOuterAlt(localctx, 3)
                self.state = 88
                self.match(VPLParser.T__16)
                self.state = 89
                self.match(VPLParser.T__2)
                self.state = 90
                self.e()
                self.state = 91
                self.match(VPLParser.T__4)
                self.state = 92
                self.e()
                self.state = 93
                self.match(VPLParser.T__3)
                pass
            elif token in [VPLParser.T__17]:
                self.enterOuterAlt(localctx, 4)
                self.state = 95
                self.match(VPLParser.T__17)
                self.state = 96
                self.match(VPLParser.T__2)
                self.state = 97
                self.e()
                self.state = 98
                self.match(VPLParser.T__4)
                self.state = 99
                self.e()
                self.state = 100
                self.match(VPLParser.T__3)
                pass
            elif token in [VPLParser.T__18]:
                self.enterOuterAlt(localctx, 5)
                self.state = 102
                self.match(VPLParser.T__18)
                self.state = 103
                self.match(VPLParser.T__2)
                self.state = 104
                self.e()
                self.state = 105
                self.match(VPLParser.T__4)
                self.state = 106
                self.e()
                self.state = 107
                self.match(VPLParser.T__3)
                pass
            elif token in [VPLParser.T__2]:
                self.enterOuterAlt(localctx, 6)
                self.state = 109
                self.match(VPLParser.T__2)
                self.state = 110
                self.e()
                self.state = 111
                self.match(VPLParser.T__3)
                pass
            elif token in [VPLParser.IDENT]:
                self.enterOuterAlt(localctx, 7)
                self.state = 113
                self.match(VPLParser.IDENT)
                pass
            elif token in [VPLParser.NUM]:
                self.enterOuterAlt(localctx, 8)
                self.state = 114
                self.match(VPLParser.NUM)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class CContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def e(self):
            return self.getTypedRuleContext(VPLParser.EContext,0)


        def NUM(self):
            return self.getToken(VPLParser.NUM, 0)

        def getRuleIndex(self):
            return VPLParser.RULE_c

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterC" ):
                listener.enterC(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitC" ):
                listener.exitC(self)




    def c(self):

        localctx = VPLParser.CContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_c)
        try:
            self.state = 125
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,6,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 117
                self.e()
                self.state = 118
                self.match(VPLParser.T__19)
                self.state = 119
                self.match(VPLParser.NUM)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 121
                self.e()
                self.state = 122
                self.match(VPLParser.T__20)
                self.state = 123
                self.match(VPLParser.NUM)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx



    def sempred(self, localctx:RuleContext, ruleIndex:int, predIndex:int):
        if self._predicates == None:
            self._predicates = dict()
        self._predicates[5] = self.s_sempred
        pred = self._predicates.get(ruleIndex, None)
        if pred is None:
            raise Exception("No predicate with index:" + str(ruleIndex))
        else:
            return pred(localctx, predIndex)

    def s_sempred(self, localctx:SContext, predIndex:int):
            if predIndex == 0:
                return self.precpred(self._ctx, 3)
         




