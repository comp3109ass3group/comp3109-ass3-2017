# Generated from VPL.g4 by ANTLR 4.7
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\32")
        buf.write("\u00a1\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write("\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r")
        buf.write("\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23")
        buf.write("\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30")
        buf.write("\4\31\t\31\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\4\3\4")
        buf.write("\3\5\3\5\3\6\3\6\3\7\3\7\3\7\3\7\3\b\3\b\3\t\3\t\3\t\3")
        buf.write("\n\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\13\3\13\3\f\3")
        buf.write("\f\3\f\3\f\3\f\3\f\3\r\3\r\3\r\3\16\3\16\3\16\3\16\3\16")
        buf.write("\3\16\3\16\3\16\3\16\3\17\3\17\3\20\3\20\3\20\3\20\3\21")
        buf.write("\3\21\3\21\3\21\3\21\3\21\3\22\3\22\3\22\3\22\3\22\3\23")
        buf.write("\3\23\3\23\3\23\3\24\3\24\3\24\3\24\3\25\3\25\3\26\3\26")
        buf.write("\3\26\3\27\3\27\7\27\u0089\n\27\f\27\16\27\u008c\13\27")
        buf.write("\3\30\6\30\u008f\n\30\r\30\16\30\u0090\3\30\3\30\6\30")
        buf.write("\u0095\n\30\r\30\16\30\u0096\5\30\u0099\n\30\3\31\6\31")
        buf.write("\u009c\n\31\r\31\16\31\u009d\3\31\3\31\2\2\32\3\3\5\4")
        buf.write("\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17")
        buf.write("\35\20\37\21!\22#\23%\24\'\25)\26+\27-\30/\31\61\32\3")
        buf.write("\2\6\5\2C\\aac|\6\2\62;C\\aac|\3\2\62;\5\2\13\f\17\17")
        buf.write("\"\"\2\u00a5\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3")
        buf.write("\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2")
        buf.write("\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2")
        buf.write("\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2")
        buf.write("#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2")
        buf.write("\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\3\63\3\2\2\2\58\3")
        buf.write("\2\2\2\7<\3\2\2\2\t>\3\2\2\2\13@\3\2\2\2\rB\3\2\2\2\17")
        buf.write("F\3\2\2\2\21H\3\2\2\2\23K\3\2\2\2\25P\3\2\2\2\27V\3\2")
        buf.write("\2\2\31\\\3\2\2\2\33_\3\2\2\2\35h\3\2\2\2\37j\3\2\2\2")
        buf.write("!n\3\2\2\2#t\3\2\2\2%y\3\2\2\2\'}\3\2\2\2)\u0081\3\2\2")
        buf.write("\2+\u0083\3\2\2\2-\u0086\3\2\2\2/\u008e\3\2\2\2\61\u009b")
        buf.write("\3\2\2\2\63\64\7h\2\2\64\65\7w\2\2\65\66\7p\2\2\66\67")
        buf.write("\7e\2\2\67\4\3\2\2\289\7g\2\29:\7p\2\2:;\7f\2\2;\6\3\2")
        buf.write("\2\2<=\7*\2\2=\b\3\2\2\2>?\7+\2\2?\n\3\2\2\2@A\7.\2\2")
        buf.write("A\f\3\2\2\2BC\7x\2\2CD\7c\2\2DE\7t\2\2E\16\3\2\2\2FG\7")
        buf.write("=\2\2G\20\3\2\2\2HI\7k\2\2IJ\7h\2\2J\22\3\2\2\2KL\7v\2")
        buf.write("\2LM\7j\2\2MN\7g\2\2NO\7p\2\2O\24\3\2\2\2PQ\7g\2\2QR\7")
        buf.write("p\2\2RS\7f\2\2ST\7k\2\2TU\7h\2\2U\26\3\2\2\2VW\7y\2\2")
        buf.write("WX\7j\2\2XY\7k\2\2YZ\7n\2\2Z[\7g\2\2[\30\3\2\2\2\\]\7")
        buf.write("f\2\2]^\7q\2\2^\32\3\2\2\2_`\7g\2\2`a\7p\2\2ab\7f\2\2")
        buf.write("bc\7y\2\2cd\7j\2\2de\7k\2\2ef\7n\2\2fg\7g\2\2g\34\3\2")
        buf.write("\2\2hi\7?\2\2i\36\3\2\2\2jk\7c\2\2kl\7f\2\2lm\7f\2\2m")
        buf.write(" \3\2\2\2no\7o\2\2op\7k\2\2pq\7p\2\2qr\7w\2\2rs\7u\2\2")
        buf.write("s\"\3\2\2\2tu\7o\2\2uv\7w\2\2vw\7n\2\2wx\7v\2\2x$\3\2")
        buf.write("\2\2yz\7f\2\2z{\7k\2\2{|\7x\2\2|&\3\2\2\2}~\7o\2\2~\177")
        buf.write("\7k\2\2\177\u0080\7p\2\2\u0080(\3\2\2\2\u0081\u0082\7")
        buf.write(">\2\2\u0082*\3\2\2\2\u0083\u0084\7@\2\2\u0084\u0085\7")
        buf.write("?\2\2\u0085,\3\2\2\2\u0086\u008a\t\2\2\2\u0087\u0089\t")
        buf.write("\3\2\2\u0088\u0087\3\2\2\2\u0089\u008c\3\2\2\2\u008a\u0088")
        buf.write("\3\2\2\2\u008a\u008b\3\2\2\2\u008b.\3\2\2\2\u008c\u008a")
        buf.write("\3\2\2\2\u008d\u008f\t\4\2\2\u008e\u008d\3\2\2\2\u008f")
        buf.write("\u0090\3\2\2\2\u0090\u008e\3\2\2\2\u0090\u0091\3\2\2\2")
        buf.write("\u0091\u0098\3\2\2\2\u0092\u0094\7\60\2\2\u0093\u0095")
        buf.write("\t\4\2\2\u0094\u0093\3\2\2\2\u0095\u0096\3\2\2\2\u0096")
        buf.write("\u0094\3\2\2\2\u0096\u0097\3\2\2\2\u0097\u0099\3\2\2\2")
        buf.write("\u0098\u0092\3\2\2\2\u0098\u0099\3\2\2\2\u0099\60\3\2")
        buf.write("\2\2\u009a\u009c\t\5\2\2\u009b\u009a\3\2\2\2\u009c\u009d")
        buf.write("\3\2\2\2\u009d\u009b\3\2\2\2\u009d\u009e\3\2\2\2\u009e")
        buf.write("\u009f\3\2\2\2\u009f\u00a0\b\31\2\2\u00a0\62\3\2\2\2\b")
        buf.write("\2\u008a\u0090\u0096\u0098\u009d\3\b\2\2")
        return buf.getvalue()


class VPLLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    T__0 = 1
    T__1 = 2
    T__2 = 3
    T__3 = 4
    T__4 = 5
    T__5 = 6
    T__6 = 7
    T__7 = 8
    T__8 = 9
    T__9 = 10
    T__10 = 11
    T__11 = 12
    T__12 = 13
    T__13 = 14
    T__14 = 15
    T__15 = 16
    T__16 = 17
    T__17 = 18
    T__18 = 19
    T__19 = 20
    T__20 = 21
    IDENT = 22
    NUM = 23
    WS = 24

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'func'", "'end'", "'('", "')'", "','", "'var'", "';'", "'if'", 
            "'then'", "'endif'", "'while'", "'do'", "'endwhile'", "'='", 
            "'add'", "'minus'", "'mult'", "'div'", "'min'", "'<'", "'>='" ]

    symbolicNames = [ "<INVALID>",
            "IDENT", "NUM", "WS" ]

    ruleNames = [ "T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", 
                  "T__7", "T__8", "T__9", "T__10", "T__11", "T__12", "T__13", 
                  "T__14", "T__15", "T__16", "T__17", "T__18", "T__19", 
                  "T__20", "IDENT", "NUM", "WS" ]

    grammarFileName = "VPL.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


