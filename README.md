# COMP3109 Assignment 3
## This is a Mini-Compiler which translates a small Vector Programming Language into Assembly Code.
### Implemented in Python as per the assignment requirements.
The resulting file can be used in C stubs. Examples of use are in the test/ directory.

# Instructions on ANTLR
You need two things in order to run ANTLR with the targeting language being Python
1. ANTLR jar
* The jar can be downloaded here
* http://www.antlr.org/download/antlr-4.7-complete.jar
2. Python ANTLR Runtime
* Use the command
```angular2html
pip install antlr4-python2-runtime
pip install antlr4-python3-runtime
```
## Steps to setup ANTLR
1. Save the path for the downloaded ANTLR jar file in an environment variable
e.g. (if the jar is in /usr/local/lib/)
```angular2html
export ANTLRPATH="/usr/local/lib/antlr-4.7-complete.jar"
```
Alternatively, **edit ~/.bast_profile** and add the line above
2. Set up an alias for the antlr run command
### Mac
```angular2html
alias antlr4='java -Xmx500M -cp $ANTLRPATH org.antlr.v4.Tool'
alias grun='java org.antlr.v4.gui.TestRig'
```
### WINDOWS
https://tomassetti.me/antlr-mega-tutorial/#setup-antlr
