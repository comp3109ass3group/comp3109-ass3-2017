import sys
from antlr4 import *
from VPLLexer import VPLLexer
from VPLParser import VPLParser


def main(argv):
    input = FileStream(argv[1])
    lexer = VPLLexer(input)
    stream = CommonTokenStream(lexer)
    parser = VPLParser(stream)

    tree = parser.m()
    print(tree.toStringTree(recog=parser))

if __name__ == '__main__':
    main(sys.argv)
